# SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
# SPDX-License-Identifier: GPL-3.0-or-later

option (
  'devel',
  type: 'boolean',
  value: 'false',
  description: 'Build the app with the development style'
)
