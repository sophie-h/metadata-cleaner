<?xml version="1.0" encoding="UTF-8"?>
<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<page xmlns="http://projectmallard.org/1.0/"
	xmlns:its="http://www.w3.org/2005/11/its"
	type="topic"
	id="usage">
	<section id="adding-files">
		<info>
			<link type="guide" xref="index#usage"/>
		</info>
		<title>Adding files</title>
		<p>To add files, press the <gui style="button">Add Files</gui> button. A file chooser will open, select the files you want to clean.</p>
		<figure>
			<title><gui style="button">Add Files</gui> button</title>
			<media type="image" src="figures/add-files-button.png" its:translate="no"/>
		</figure>
		<p>To add whole folders at once, press the arrow next to the <gui style="button">Add Files</gui> button and press the <gui style="button">Add Folders</gui> button. A file chooser will open, select the folders you want to add. You can optionally choose to also add all files from all subfolders by checking the <gui style="checkbox">Add files from subfolders</gui> checkbox.</p>
	</section>
	<section id="viewing-metadata">
		<info>
			<link type="guide" xref="index#usage"/>
		</info>
		<title>Viewing metadata</title>
		<p>Click on a file in the list to open the detailed view. If it has metadata, it will be shown there.</p>
		<figure>
			<title>Detailed view of the metadata</title>
			<media type="image" src="figures/metadata-example.png" its:translate="no"/>
		</figure>
	</section>
	<section id="cleaning-files">
		<info>
			<link type="guide" xref="index#usage"/>
		</info>
		<title>Cleaning files</title>
		<p>To clean all the files in the window, press the <gui style="button">Clean</gui> button. The cleaning process may take some time to complete.</p>
		<figure>
			<title><gui style="button">Clean</gui> button</title>
			<media type="image" src="figures/clean-button.png" its:translate="no"/>
		</figure>
	</section>
	<section id="lightweight-cleaning">
		<info>
			<link type="guide" xref="index#usage"/>
		</info>
		<title>Lightweight cleaning</title>
		<p>By default, the removal process might alter a bit the data of your files, in order to remove as much metadata as possible. For example, texts in PDF might not be selectable anymore, compressed images might get compressed again…</p>
		<p>The lightweight mode, while not as thorough, will not make destructive changes to your files.</p>
	</section>
</page>
