# SPDX-FileCopyrightText: 2020, 2021 Romain Vigier <contact AT romainvigier.fr>
# SPDX-License-Identifier: GPL-3.0-or-later

variables:
  APP_ID: fr.romainvigier.MetadataCleaner
  MANIFEST: build-aux/fr.romainvigier.MetadataCleaner.yaml
  PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/metadata-cleaner

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - check
  - build
  - bundle
  - deploy

.python:
  image: python:latest
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    key: python
    paths:
      - .cache/pip
      - venv/
  before_script:
    - python -V
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate

mypy:
  extends: .python
  stage: check
  interruptible: true
  rules:
    - changes:
      - application/**/*.py
  script:
    - pip install mypy
    - mypy --ignore-missing-imports ./application

pydocstyle:
  extends: .python
  stage: check
  interruptible: true
  rules:
    - changes:
      - application/**/*.py
  script:
    - pip install pydocstyle
    - pydocstyle ./application

pycodestyle:
  extends: .python
  stage: check
  interruptible: true
  rules:
    - changes:
      - application/**/*.py
  script:
    - pip install pycodestyle
    - pycodestyle ./application

reuse:
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  stage: check
  interruptible: true
  script:
    - reuse lint

flatpak-repo:
  stage: build
  interruptible: true
  needs:
    - job: mypy
      optional: true
    - job: pydocstyle
      optional: true
    - job: pycodestyle
      optional: true
    - reuse
  image: registry.gitlab.com/rmnvgr/metadata-cleaner/build-env:latest
  cache:
    - key: flatpak-repo
      paths:
        - .flatpak-builder
  script:
    - if [[ -v CI_COMMIT_TAG ]]; then build-aux/prepare_release.py; fi
    - flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    - flatpak-builder --install-deps-from=flathub --repo=repo --default-branch=$CI_COMMIT_REF_SLUG --ccache --disable-rofiles-fuse builddir $MANIFEST
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-flatpak-repo
    paths:
      - repo
    expire_in: 1 day

app:
  stage: bundle
  interruptible: true
  needs:
    - job: flatpak-repo
      artifacts: true
  image: registry.gitlab.com/rmnvgr/metadata-cleaner/build-env:latest
  script:
    - flatpak build-bundle ./repo $APP_ID.flatpak $APP_ID $CI_COMMIT_REF_SLUG
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-flatpak
    expose_as: Flatpak bundle
    paths:
      - fr.romainvigier.MetadataCleaner.flatpak
    expire_in: 1 day

debug:
  stage: bundle
  interruptible: true
  needs:
    - job: flatpak-repo
      artifacts: true
  image: registry.gitlab.com/rmnvgr/metadata-cleaner/build-env:latest
  script:
    - flatpak build-bundle --runtime ./repo $APP_ID.Debug.flatpak $APP_ID.Debug $CI_COMMIT_REF_SLUG
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG-flatpak-debug
    expose_as: Flatpak debug symbols
    paths:
      - fr.romainvigier.MetadataCleaner.Debug.flatpak
    expire_in: 1 day

package:
  stage: deploy
  needs:
    - job: app
      artifacts: true
    - job: debug
      artifacts: true
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl \
      --header "JOB-TOKEN: $CI_JOB_TOKEN" \
      --upload-file ${APP_ID}.flatpak \
      "${PACKAGE_REGISTRY_URL}/$CI_COMMIT_TAG/${APP_ID}.flatpak"
    - |
      curl \
      --header "JOB-TOKEN: $CI_JOB_TOKEN" \
      --upload-file ${APP_ID}.Debug.flatpak \
      "${PACKAGE_REGISTRY_URL}/$CI_COMMIT_TAG/${APP_ID}.Debug.flatpak"

release-notes:
  extends: .python
  stage: deploy
  needs:
    - reuse
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - ./build-aux/get_release_notes.py $CI_COMMIT_TAG > release-notes.md
  artifacts:
    paths:
      - release-notes.md
    expire_in: 1 day

release:
  stage: deploy
  needs:
    - package
    - job: release-notes
      artifacts: true
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Creating release for $CI_COMMIT_TAG"
  release:
    name: Metadata Cleaner $CI_COMMIT_TAG
    tag_name: $CI_COMMIT_TAG
    description: release-notes.md
    assets:
      links:
        - name: Flatpak bundle
          url: ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${APP_ID}.flatpak
          filepath: /${APP_ID}.flatpak
          link_type: package

screenshots:
  stage: deploy
  needs:
    - reuse
  image: registry.gitlab.com/rmnvgr/metadata-cleaner/screenshot-env:latest
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
      - application/po/*.po
      - help/LINGUAS
      - screenshots/**/*
  before_script:
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git remote set-url origin "https://${CI_GITLAB_USERNAME}:${CI_GITLAB_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
  script:
    - python3 screenshots/screenshot.py
  after_script:
    - git status
    - |
      if [[ -n $(git status --porcelain resources) ]]; then
        git add resources
        git commit -m "Resources: Update screenshots"
      fi
    - |
      if [[ -n $(git status --porcelain help) ]]; then
        git add help
        git commit -m "Help: Update screenshots"
      fi
    - |
      if [[ -n $(git status --porcelain website) ]]; then
        git add website
        git commit -m "Website: Update screenshots"
      fi
    - git push origin HEAD:main

pages:
  stage: deploy
  needs:
    - reuse
  image: registry.gitlab.com/rmnvgr/metadata-cleaner/build-env:latest
  script:
    - cp -r website public
    - cp -r resources/screenshots public/screenshots
    - find public -type f -regex '.*\.\(|html\|css\|svg\|png\|woff\|woff2\)$' -exec gzip -f -k {} \;
    - find public -type f -regex '.*\.\(|html\|css\|svg\|png\|woff\|woff2\)$' -exec brotli -f -k {} \;
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
      - resources/screenshots/**/*
      - website/**/*
  artifacts:
    paths:
      - public
    expire_in: 1 day
